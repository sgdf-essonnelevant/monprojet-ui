import json
import os.path


def isDataBasePresent(dbName):
    return os.path.isfile(dbName)


def charger_donnees(bddName):
    try:
        with open(bddName, 'r') as f:
            return json.load(f)
    except FileNotFoundError:
        return []


# Fonction pour sauvegarder les données dans le fichier JSON
def sauvegarder_donnees(data, bddName):
    with open(bddName, 'w') as f:
        json.dump(data, f, indent=4)


def ajouter_donnees(data_to_add, bddName):
    data = charger_donnees(bddName)
    data.extend(data_to_add)
    sauvegarder_donnees(data, bddName)


def delete_data(bddName):
    sauvegarder_donnees("", bddName)
