from http.client import IncompleteRead

import streamlit as st
from requests import Session
import json
import requests
from bs4 import BeautifulSoup
import bdd_json
import time
from random import randint
import base64


class MySession:
    def __init__(self, client):
        self.token = None
        self.client = client
        self.userData = None

    def get_token(self):
        return self.token

    def get_userData(self):
        return self.userData

    def login(self, login, password):
        # if not credentials.is_valid():
        #     raise Exception("InvalidCredentials")
        if not self.token:
            try:
                print("Requesting a new token")
                response = self.client.get("https://monprojet.sgdf.fr/api/login")
                soup = BeautifulSoup(response.content, "html.parser")
                form = soup.select_one("form")
                auth_url = form.get("action")

                form_data = {
                    "username": login,
                    "password": password,
                    "credentialId": ""
                }

                response = self.client.post(auth_url, data=form_data)
                login_loc = "https://monprojet.sgdf.fr/api/login/check?" + response.url.split("?")[1]
                print("Login check")
                response = self.client.get(login_loc)
                print(response)
                print(response.content)
                login = json.loads(response.content)
                self.token = login['token']
                self.userData = login['userData']
                print("New token acquired")
            except Exception as e:
                print(e)
                return False
        # print("token : " + self.token)
        return True

    def query_ddc_project(self, query):
        url = "https://monprojet.sgdf.fr/api/ddc-projets"
        headers = {
            "Authorization": "Bearer " + self.token,
            "Origin": "https://monprojet.sgdf.fr"
        }
        response = self.client.get(url, headers=headers)
        adherents = json.loads(response.content)
        print(adherents)
        return adherents

    def logout(self):
        st.session_state.clear()
        st.experimental_rerun()
        st.success("You have been logged out successfully!")


    def query_camp(self, token):
        # bdd_json.delete_data()
        numPage = 1
        camps = []
        response = self.fetch_camp_next_page(token, numPage)
        if response is None:
            print(response)
            st.toast("Impossible de récuperer le camp sur monProjet")
        camps.extend(response["camps"])
        # bdd_json.sauvegarder_donnees(camps["camps"])

        nbCampFetched = len(camps)
        while nbCampFetched < response["campsTotalCount"]:
            numPage += 1
            response = self.fetch_camp_next_page(token, numPage)
            camps.extend(response["camps"])
            nbCampFetched =len(camps)
            # bdd_json.ajouter_donnees(camps["camps"])

        return camps

    def fetch_camp_next_page(self, token, numPage) -> dict:
        print("Fetching page " + str(numPage))
        url = "https://monprojet.sgdf.fr/api/camps/multi-criteres?dateDebut=2024-06-01T00:00:00.000Z&dateFin=2024-08-31T00:00:00.000Z&chercherDossiersParticipants=0&codeStructure=119120000&chercherStructuresDependates=1&idsTamRefExercices=19&selectedPage=" + str(
            numPage)
        headers = {
            "Authorization": "Bearer " + token,
            "Origin": "https://monprojet.sgdf.fr"
        }
        response = self.requestWithRetry(url,headers)
        camps = json.loads(response.content)
        return camps

    def requestWithRetry(self, url, headers):
        max_retries = 3  # Nombre maximal de tentatives de réessayer la requête
        retries = 0
        success = False
        response = None

        while retries < max_retries and not success:
            try:
                response = self.client.get(url, headers=headers)
                response.raise_for_status()
                if response is not None:
                    success = True
            except IncompleteRead as e:
                print("Erreur de lecture de la réponse :", e)
                retries += 1
                if retries < max_retries:
                    print("Tentative de réessayer la requête...")
                    time.sleep(1)
            except requests.exceptions.RequestException as e:
                print("Erreur lors de la requête :", e)
                break

        if success:
            print("Requête réussie")
        else:
            print("Échec de la requête après {} tentatives.".format(max_retries))
        return response

    def fetch_camp_module(self, token, campId, moduleName) -> dict:
        print(f"Fetching camp {str(campId)} module {moduleName} ")
        url = f"https://monprojet.sgdf.fr/api/camps/{campId}?module={moduleName}"
        headers = {
            "Authorization": "Bearer " + token,
            "Origin": "https://monprojet.sgdf.fr"
        }
        response = self.requestWithRetry(url, headers=headers)
        # if response is None:
        #     self.logout()
        print(response)
        moduleDetail = json.loads(response.content)
        return moduleDetail

    def fetch_and_save_base64_data(self, token, id, output_file_path):
        try:
            url = f"https://monprojet.sgdf.fr/api/ddc-fichiers/download/{id}?typeDdc=CAMP"
            headers = {
                "Authorization": "Bearer " + token,
                "Origin": "https://monprojet.sgdf.fr"
            }
            response= self.requestWithRetry(url, headers)
            # base64_data = response.content.decode('utf-8')
            # decoded_data = base64.b64decode(response.content.text)
            with open(output_file_path, 'wb') as file:
                file.write(response.content)

            print(f"Les données ont été sauvegardées avec succès dans {output_file_path}")
        except requests.exceptions.RequestException as e:
            print(f"Erreur lors de la requête : {e}")
        except base64.binascii.Error as e:
            print(f"Erreur lors du décodage des données base64 : {e}")

# login_sucess=False
# while not login_sucess:
#     login_sucess=False
# while not login_sucess:
#client = Session()
#     session = MySession(client)
#     time.sleep(randint(10,300))
#     print("login succes "+ str(login_sucess))
#     print(session.token)
# print(session.userData)

# session.query_camp("j")
