import streamlit as st
import time
import numpy as np
import bdd_json
from datetime import datetime
from geopy.geocoders import Nominatim
from geopy.distance import geodesic


from home import getBddName,getModule, filterByBranch,getVille
# st.set_page_config(page_title="Plotting Demo", page_icon="📈")

st.markdown("# Tableau de bord")
st.sidebar.header("Plotting Demo")

dataFromJson = bdd_json.charger_donnees(getBddName())

def displayInfoAboutMax(camps):
    maxDuration = 0
    maxDurationLibelle = ""
    maxDistance = 0
    maxDistanceLibelle=""
    maxNbAdherent=0
    maxNbAdherentLibelle=""
    for camp in camps:
        campId = camp['id']
        # print(f"Chargement du camp {camp['libelle']} {campId}")
        infoGeneral = getModule(campId, "INFO_GENERALE")
        dateDebut = infoGeneral["dateDebut"]
        dateFin = infoGeneral["dateFin"]
        date_debut = datetime.fromisoformat(dateDebut.split('T')[0])
        date_fin = datetime.fromisoformat(dateFin.split('T')[0])
        differenceDate = date_fin - date_debut
        if differenceDate.days > maxDuration:
            maxDuration = differenceDate.days
            maxDurationLibelle = infoGeneral['libelle']

        lieu = getVille(infoGeneral,"")
        distance = getDistance(lieu)
        if distance> maxDistance:
            maxDistance= distance
            maxDistanceLibelle=infoGeneral['libelle']

        participantsModule = getModule(campId, "PARTICIPANT")
        nbAdherent = len(participantsModule['campAdherentParticipants'])
        if nbAdherent > maxNbAdherent:
            maxNbAdherent = nbAdherent
            maxNbAdherentLibelle = infoGeneral['libelle']
    st.markdown(f"**Durée de camp la plus grande** : {maxDuration} jours : {maxDurationLibelle}")
    st.markdown(f"**Distance la plus grande** : {maxDistance:.2f} km : {maxDistanceLibelle}")
    st.markdown(f"**Plus grand effectif** : {maxNbAdherent} jeunes: {maxNbAdherentLibelle}")


@st.cache_data
def getDistance(lieu):
    distance=0
    try :
        if lieu is not None:
            geolocator = Nominatim(user_agent="geoapiExercises")
            # geolocator = OpenCage(api_key=None)

            location1 = geolocator.geocode("Vert-le-Grand, 91810")
            location2 = geolocator.geocode(lieu)

            if location2 is not None:
                # Extraire les coordonnées (latitude et longitude)
                coords_1 = (location1.latitude, location1.longitude)
                coords_2 = (location2.latitude, location2.longitude)

                # Calculer la distance entre les deux points
                distance = geodesic(coords_1, coords_2).kilometers
                print(f"La distance entre les adresses {location1} et {location2} est de {distance:.2f} km")
            else:
                print(f"Impossible d'avoir les coordonnées du lieu {lieu}")
    except Exception as e:
            print(e)
    return distance

st.markdown("# Farfadets")
displayInfoAboutMax(filterByBranch("FARFADET"))

st.markdown("# Louveteaux-Jeannettes")
displayInfoAboutMax(filterByBranch("8-11"))

st.markdown("# Scouts-Guides")
displayInfoAboutMax(filterByBranch("11-14"))

st.markdown("# Pionniers caravelles")
displayInfoAboutMax(filterByBranch("14-17"))

st.markdown("# Compagnons")
displayInfoAboutMax(filterByBranch("COMPAGNON"))