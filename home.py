import streamlit as st
import pandas as pd
import bdd_json
from datetime import datetime
from requests import Session
import sessionTest
import os
import shutil
import locale

# from geopy.geocoders import Nominatim
# from geopy.distance import geodesic
from st_aggrid import GridOptionsBuilder, AgGrid, GridUpdateMode, ColumnsAutoSizeMode, JsCode
from streamlit_cookies_controller import CookieController

st.set_page_config(
    layout="wide"
)
session = sessionTest.MySession(Session())
cookie = CookieController()


def check_password():
    """Returns `True` if the user had a correct password."""
    print("check password begin")

    def login_form():
        """Form with widgets to collect user information"""
        with st.form("Credentials"):
            st.text_input("Username", key="username")
            st.text_input("Password", type="password", key="password")
            st.form_submit_button("Log in", on_click=password_entered)

    def password_entered():
        """Checks whether a password entered by the user is correct."""
        if session.login(st.session_state["username"], st.session_state["password"]):
            st.session_state["token"] = session.get_token()
            cookie.set("token", session.get_token())
            cookie.set("username", st.session_state["username"])
            st.session_state["password_correct"] = True
            del st.session_state["password"]  # Don't store the username or password.
            # del st.session_state["username"]
        else:
            st.session_state["password_correct"] = False

    # Return True if the username + password is validated.
    if (cookie.get("token") is not None
            or st.session_state.get("password_correct", False)):
        return True

    # Show inputs for username + password.
    login_form()
    if "password_correct" in st.session_state:
        st.error("😕 User not known or password incorrect")
    return False


if not check_password():
    st.stop()


def getBddName():
    return str(cookie.get("username")) + ".json"


def loadCamp(bddName):
    fetched_camps = session.query_camp(st.session_state["token"])
    bdd_json.sauvegarder_donnees(fetched_camps, bddName)


fileSaveName = getBddName()
if bdd_json.isDataBasePresent(fileSaveName):
    print("present " + fileSaveName)
else:
    print("pas present " + fileSaveName)
    fetched_camps = session.query_camp(st.session_state["token"])
    bdd_json.sauvegarder_donnees(fetched_camps, fileSaveName)

dataFromJson = bdd_json.charger_donnees(fileSaveName)
pd.options.display.float_format = '{:,.2f}'.format


def filterByBranch(branchName):
    camps = []
    for camp in dataFromJson:
        if "A SUPPRIMER" not in camp['libelle'] and (camp['typeCamp']['code'] == branchName or camp['typeCamp']['codeGroupeCamp'] == branchName):
            camps.append(camp)
    return camps


def getByBranch(branchName):
    camps = []

    for camp in dataFromJson:
        if camp['typeCamp']['code'] == branchName or camp['typeCamp']['codeGroupeCamp'] == branchName:
            camps.append(getRadioLabel(camp))
            # camps.append(camp['libelle'])
    return camps


def getRadioLabel(camp):
    branchName = camp['typeCamp']['code']
    color = getColorBranch(branchName)
    return f":{color}[{camp['libelle']}]"


def getColorBranch(branchName):
    color = "orange" if branchName == "8-11" else "blue" if branchName == "11-14" else "red" if branchName == "14-17" else "violet" if branchName == "AUDACE" else "green" if branchName == "COMPAGNONS-T1" or branchName == "COMPAGNONS-T2" or branchName == "FARFADET" else "black"
    return color


def getModule(campId, moduleName):
    with st.spinner('Récuperation depuis MonProjet...'):
        campId = str(campId)
        if not os.path.exists(campId):
            os.makedirs(campId)
        fileSaveName = str(campId) + "/" + moduleName
        if not bdd_json.isDataBasePresent(fileSaveName):
            campDetail = session.fetch_camp_module(st.session_state["token"], campId, moduleName)
            bdd_json.sauvegarder_donnees(campDetail, fileSaveName)
        return bdd_json.charger_donnees(fileSaveName)


with st.sidebar:
    if st.button("Se déconnecter"):
        session.logout()

    radioOption = {}

    selectedBranch = []

    if st.checkbox("Farfadets"):
        selectedBranch.extend(getByBranch("FARFADET"))
    if st.checkbox("Louveteaux-Jeannettes"):
        selectedBranch.extend(getByBranch("8-11"))
    if st.checkbox("Scouts-Guides"):
        selectedBranch.extend(getByBranch("11-14"))
    if st.checkbox("Pionniers-Caravelles"):
        selectedBranch.extend(getByBranch("14-17"))
    if st.checkbox("Compagnons"):
        selectedBranch.extend(getByBranch("COMPAGNON"))
    if st.checkbox("Audaces"):
        selectedBranch.extend(getByBranch("AUDACE"))

    option = st.radio(f"Sélectionnez un camp ({len(selectedBranch)}) :", selectedBranch, index=None)

    for camp in dataFromJson:
        radioOption[getRadioLabel(camp)] = camp
    # # option = st.radio("Sélectionnez un camp :", list(radioOption.keys()))
    campId = None
    if option is not None:
        campId = radioOption[option]['id']
    if st.button("Recharger depuis MonProjet"):
        try:
            shutil.rmtree(getBddName())
            loadCamp(getBddName())
        except Exception as e:
            print(e)
            st.toast(":red[Impossible de recharger depuis MonProjet]")
    if st.button("Recharger tous les camp"):
        try:
            for camp in dataFromJson:
                campId = camp['id']
                print(f"Chargement du camp {campId}")
                getModule(campId, "INFO_GENERALE")
                getModule(campId, "PARTICIPANT")
                getModule(campId, "PROJET_PEDA")
                getModule(campId, "ENTETE")
                getModule(campId, "MENU")
                getModule(campId, "JOURNEE_TYPE")
                getModule(campId, "GRILLE_ACTIVITE")
                print(f"Chargement du camp {campId} terminé")

        except Exception as e:
            print(e)
            st.toast(":red[Impossible de recharger depuis MonProjet]")


@st.experimental_dialog("Detail adherent")
def displayAdherent(adherent):
    st.write(f"**Numéro :** {adherent['numero']}")
    st.write(f"**Prénom :** {adherent['prenom']}")
    st.write(f"**Nom :** {adherent['nom']}")
    st.write(f"**Nom de Naissance :** {adherent['nomNaissance']}")
    st.write(f"**Genre :** {adherent['genre']}")
    st.write(f"**Statut :** {adherent['statut']}")
    st.write(f"**Fonction :** {adherent['fonction']}")
    st.write(f"**Lieu de Naissance :** {adherent['lieuDeNaissance']}")
    st.write(f"**Code INSEE :** {adherent['codeInsee']}")
    st.write(f"**Date de Naissance :** {adherent['dateDeNaissance']}")
    st.write(f"**Téléphone Bureau :** {adherent['telephoneBureau']}")
    st.write(f"**Téléphone Domicile :** {adherent['telephoneDomicile']}")
    st.write(f"**Téléphone Portable 1 :** {adherent['telephonePortable1']}")
    st.write(f"**Téléphone Portable 2 :** {adherent['telephonePortable2']}")
    st.write(f"**Email :** {adherent['email']}")

    # Afficher les formations de l'adhérent
    if adherent['adherentFormations']:
        st.write("### Formations")
        formations_df = pd.DataFrame(adherent['adherentFormations'])
        st.table(formations_df[['libelle', 'type', 'dateFin', 'role']])

    # Afficher les diplômes de l'adhérent (s'il y en a)
    if adherent['adherentDiplomes']:
        st.write("### Diplômes")
        diplomes_df = pd.DataFrame(adherent['adherentDiplomes'])
        st.table(diplomes_df)

    # Afficher les qualifications de l'adhérent
    if adherent['adherentQualifications']:
        st.write("### Qualifications")
        qualifications_df = pd.DataFrame(adherent['adherentQualifications'])
        st.table(qualifications_df[['type', 'dateFin', 'estTitulaire']])


@st.experimental_dialog("Detail adherent")
def displayJeune(adherent):
    st.write(f"**Numéro :** {adherent['numero']}")
    st.write(f"**Prénom :** {adherent['prenom']}")
    st.write(f"**Nom :** {adherent['nom']}")
    st.write(f"**Date de Naissance :** {adherent['dateNaissance']}")
    st.write(f"**Téléphone Bureau :** {adherent['telephonePere']}")
    st.write(f"**Téléphone Domicile :** {adherent['telephoneMere']}")
    st.write(f"**Email :** {adherent['email']}")


def format_date(date_str):
    locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')

    date_obj = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S.%f%z")
    formatted_date = date_obj.strftime("%A %d/%m")

    return formatted_date


def getLieu(campId):
    infoGeneral = getModule(campId, "INFO_GENERALE")
    lieu = ""
    if infoGeneral['campLieuPrincipal'] is not None:
        if infoGeneral["campLieuPrincipal"]["adresseLigne1"] is not None:
            lieu += infoGeneral["campLieuPrincipal"]["adresseLigne1"]
        lieu = getVille(infoGeneral, lieu)
        if not lieu:
            lieu = "Pas de lieu"
    return lieu


def getVille(infoGeneral, lieu):
    if infoGeneral["campLieuPrincipal"]["tamRefCommune"] is not None and infoGeneral["campLieuPrincipal"]["tamRefCommune"]["libelle"] is not None:
        lieu += ", " + infoGeneral["campLieuPrincipal"]["tamRefCommune"]["libelle"]
    if infoGeneral["campLieuPrincipal"]["tamRefCommune"] is not None and infoGeneral["campLieuPrincipal"]["tamRefCommune"]["codePostale"] is not None:
        lieu += ", " + infoGeneral["campLieuPrincipal"]["tamRefCommune"]["codePostale"]
    return lieu


if campId is not None:
    # staff = getModule(campId, "STAFF")
    # spi = getModule(campId, "SPI")
    if st.button("Recharger depuis MonProjet", "deleteCurrentCampDirectory"):
        shutil.rmtree(str(campId))

    tabInfo, tabMaitrise, tabJeunes, tabPeda, tabMethode, tabJournee, tabActivite, tabSpi, tabBudget, tabMenu \
        = st.tabs(["Infos", "Maitrise", "Jeunes", "Projet péda", "Méthode scoute", "Journée type", "Activités", "Spi", "Budget", "Menus"])

    # Info général
    with tabInfo:

        st.markdown(f"**Lien** : *https://monprojet.sgdf.fr/camp/{campId}*")
        lieu = getLieu(campId)
        st.markdown(f"**Lieu** : {lieu}")

        infoGeneral = getModule(campId, "INFO_GENERALE")
        dateDebut = infoGeneral["dateDebut"]
        dateFin = infoGeneral["dateFin"]
        date_debut = datetime.fromisoformat(dateDebut.split('T')[0])
        date_fin = datetime.fromisoformat(dateFin.split('T')[0])
        differenceDate = date_fin - date_debut
        st.markdown(f"**Date** : " + date_debut.strftime("%d/%m") + " - " + date_fin.strftime("%d/%m") + " (" + str(
            differenceDate.days) + " jours)")

        unites = [structure['structure']['libelle'].lower() for structure in infoGeneral['campStructures']]
        st.markdown("**Structures participantes** : " + " , ".join(unites))
        soutiens = [soutien['adherent']['prenom'].title() + " " + soutien['adherent']['nom'].title() for soutien in infoGeneral['campAdherentSoutiens']]
        st.markdown("**Soutien(s)** : " + ", ".join(soutiens))
        isJumelage = "oui" if len(infoGeneral["campStructures"]) > 1 else "non"
        st.markdown(f"**Jumelage** : {isJumelage}")

        # Maitrise
        with tabMaitrise:
            staff = infoGeneral['campAdherentStaffsInformations']
            # with st.expander(f"Maitrise ({len(staff)})"):
            for adherent in staff:
                col1, col2 = st.columns(2)
                with col1:
                    st.write(f"{adherent['prenom']} {adherent['nom']}")
                with col2:
                    if st.button(f"Détail ", adherent['numero']):
                        displayAdherent(adherent)

        # Jeunes
        with tabJeunes:
            participantsModule = getModule(campId, "PARTICIPANT")
            st.markdown(f"**Total :** {len(adherent)}")
            # with st.expander(f"Jeunes ({len(participantsModule)})"):
            for adherent in participantsModule['campAdherentParticipants']:
                col1, col2 = st.columns(2)
                with col1:
                    st.write(f"{adherent['adherent']['prenom']} {adherent['adherent']['nom']}")
                with col2:
                    if st.button(f"Détail ", adherent['adherent']['numero']):
                        displayJeune(adherent['adherent'])
    # Projet péda
    with tabPeda:
        projetPeda = getModule(campId, "PROJET_PEDA")
        with st.expander("Bilan année"):
            if projetPeda['camp817'] is not None and projetPeda['camp817']['projetPedaBilanAnnee'] is not None:
                bilanAnnee = "<html>" + projetPeda['camp817']['projetPedaBilanAnnee'] + "</html>"
                st.markdown(bilanAnnee, unsafe_allow_html=True)
            else:
                st.write("vide")
        with st.expander("Projet péda"):
            if projetPeda['camp817'] is not None and projetPeda['camp817']['projetPedaAmbitions'] is not None:
                projetPedaAmbitions = "<html>" + projetPeda['camp817']['projetPedaAmbitions'] + "</html>"
                st.markdown(projetPedaAmbitions, unsafe_allow_html=True)
            else:
                st.write("vide")

    entete = getModule(campId, "ENTETE")

    with tabMethode:
        for modif in entete['campModules']:
            if modif['module'] is not None and modif['module']['libelle'] == 'Méthode Scoute' and modif['surveyjsReponsesJson'] is not None:
                for question in modif['surveyjsReponsesJson']:
                    with st.expander(question.title()):
                        response = modif['surveyjsReponsesJson'][question]
                        st.write(response)
    with tabSpi:
        for modif in entete['campModules']:
            if modif['module'] is not None and modif['module']['libelle'] == 'SPI' and modif['surveyjsReponsesJson'] is not None:
                for question in modif['surveyjsReponsesJson']:
                    with st.expander(question.title()):
                        response = modif['surveyjsReponsesJson'][question]
                        response = "Oui" if response == "1" else "Non" if response == "2" else response
                        st.write(response)
    with tabBudget:
        for file in infoGeneral['campFichiers']:
            if file['categorie'] == "BUDGET":
                fileName = file['nom']
                filePath = str(campId) + "/" + fileName
                id = file['id']
                if not os.path.exists(filePath):
                    session.fetch_and_save_base64_data(st.session_state["token"], id, filePath)
                try:
                    df = pd.read_excel(filePath, engine='openpyxl')

                    target_value = "Coût du camp par jeune:"
                    cost_value = None

                    for index, row in df.iterrows():
                        if row.iloc[0] == target_value:
                            cost_value = row.iloc[1]
                            break

                    st.markdown(f"**Cout du camp par jeune** : {cost_value} €")

                    with st.expander("Apercu du budget"):
                        st.dataframe(df)


                except Exception as e:
                    st.error(f"Erreur lors de la lecture du fichier Excel : {e}")

                with open(filePath, "rb") as file:
                    btn = st.download_button(
                        label=f"Télécharger {fileName}",
                        data=file,
                        file_name=fileName,
                        mime="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    )
                break

    with tabMenu:
        activites = getModule(campId, "MENU")['campMenus']
        activitesTab = []
        for repa in activites:
            referents = "N/A"
            repas = "Petit déjeuner" if repa['repas'] == 1 else "Déjeuner" if repa['repas'] == 2 else "Goûter" if repa['repas'] == 3 else "Dîner"
            activitesTab.append({
                "Date": str(format_date(repa['dateMenu'])),
                "Repas": repas,
                "Description": repa['description'],
            })

        horraires = pd.DataFrame(activitesTab)
        st.dataframe(horraires, hide_index=True, use_container_width=True)

    with tabJournee:
        journeeType = getModule(campId, "JOURNEE_TYPE")['campJourneeTypes']
        horraires = pd.DataFrame(journeeType)
        st.dataframe(horraires[['heureDebut', 'activiteParticipants', 'activiteStaff']], hide_index=True, use_container_width=True)

    with tabActivite:
        activites = getModule(campId, "GRILLE_ACTIVITE")['campGrilleActivites']
        activitesTab = []
        for repa in activites:
            referents = "N/A"
            creneau = "matin" if repa['creneau'] == 1 else "Après-midi" if repa['creneau'] == 2 else "soir"
            if repa['campGrilleActiviteCampAdherentStaffs'] is not None:
                referents = [staffMember["campAdherentStaff"]["adherent"]["prenom"].title() for staffMember in repa['campGrilleActiviteCampAdherentStaffs']]
            activitesTab.append({
                "Date": str(repa['dateActivite']),
                "Créneau": creneau,
                "Description": repa['description'],
                "Imaginaire": repa['imaginaire'],
                "Référent": ", ".join(referents),
                "Objectif pédagogique": repa['objectifPedagogique']
            })

        horraires = pd.DataFrame(activitesTab)
        st.dataframe(horraires, hide_index=True, use_container_width=True)

else:
    st.write("No row selected")
